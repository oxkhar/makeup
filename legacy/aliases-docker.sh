##
# : ${DOCKER_CLI:=sudo docker}
: ${DOCKER_CLI:=docker}
: ${DOCKER_ENV_FILE:=".env.docker"}
: ${DOCKER_BUILD_FILE:="Dockerfile"}
: ${DOCKER_IMAGE_RUN:=${DOCKER_REGISTRY}${DOCKER_IMAGE_NAME:=php}:${DOCKER_IMAGE_VERSION:=latest}}
: ${DOCKER_USER_RUN:=$(id -u):$(id -g)}
: ${DOCKER_COMPOSE_PLUGIN:=" ~/.docker/cli-plugins/docker-compose"}
: ${DOCKER_COMPOSE_PATH:="${TOOLS_BIN}/docker-compose"}
: ${DOCKER_COMPOSE_CLI:="${DOCKER_COMPOSE_PATH}"}
# : ${DOCKER_COMPOSE_CLI:="${DOCKER_CLI} compose"}
: ${DOCKER_COMPOSE_FILE:=docker-compose.yaml}
: ${DOCKER_SERVICE_RUN:=cli}
: ${DOCKER_NETWORK_NAME:=bridge}
: ${DOCKER_WORKDIR:=/app}
##
DOCKER_RUN_OPTIONS+="
  --rm
  --workdir ${DOCKER_WORKDIR}
  --user ${DOCKER_USER_RUN}
  --volume /etc/group:/etc/group:ro
  --volume /etc/passwd:/etc/passwd:ro
"
DOCKER_RUN_EXTRA=""
#
DOCKER_BUILD_OPTIONS+="
  --build-arg APP_USER="$(id -u):$(id -g)"
"

##
docker-init-network() {
  : aliases-exec ${DOCKER_CLI} network create ${DOCKER_NETWORK_NAME} 2>/dev/null
}

docker-build() {
  aliases-exec ${DOCKER_CLI} build ${DOCKER_BUILD_OPTIONS} --tag ${DOCKER_IMAGE_RUN} --file ${DOCKER_BUILD_FILE} .
}

docker-run() {
  local TTY=
  tty --silent && TTY='--tty --interactive' || TTY='--attach'
  docker-init-network
  aliases-exec ${DOCKER_CLI} run ${TTY} ${DOCKER_RUN_OPTIONS} ${DOCKER_RUN_EXTRA} \
    --network ${DOCKER_NETWORK_NAME} --volume "$(pwd)":"${DOCKER_WORKDIR}" "$@" &&
    DOCKER_RUN_EXTRA=""
}

docker-compose() {
  aliases-exec ${DOCKER_COMPOSE_CLI} --env-file ${DOCKER_ENV_FILE} --file "${DOCKER_COMPOSE_FILE}" "$@"
}

docker-compose-run() {
  docker-compose run ${DOCKER_RUN_OPTIONS} ${DOCKER_RUN_EXTRA} \
    --volume "$(pwd)":"${DOCKER_WORKDIR}" "$@" &&
    DOCKER_RUN_EXTRA=""
}

docker-compose-download() {
  aliases-download ${DOCKER_COMPOSE_PATH} \
    https://github.com/docker/compose/releases/download/v${DOCKER_COMPOSE_VERSION:-2.15.1}/docker-compose-$(uname -s)-$(uname -m) ||
    return $?
  chmod +x ${DOCKER_COMPOSE_PATH}
  [ ! -e "${DOCKER_COMPOSE_PLUGIN}" ] &&
    mkdir -p "$(dirname "${DOCKER_COMPOSE_PLUGIN}")" &&
    cp "${DOCKER_COMPOSE_PATH}" "${DOCKER_COMPOSE_PLUGIN}"
}

##
aliases-var DOCKER_CLI DOCKER_BUILD_FILE DOCKER_RUN_EXTRA DOCKER_WORKDIR \
  DOCKER_IMAGE_NAME DOCKER_IMAGE_RUN DOCKER_IMAGE_VERSION DOCKER_BUILD_OPTIONS \
  DOCKER_COMPOSE_FILE DOCKER_COMPOSE_CLI DOCKER_COMPOSE_PLUGIN DOCKER_COMPOSE_PATH \
  DOCKER_RUN_OPTIONS DOCKER_SERVICE_RUN DOCKER_NETWORK_NAME DOCKER_USER_RUN

aliases-fnc docker-init-network docker-build docker-run docker-compose docker-compose-run docker-compose-download
