
help-title-shell() {
cat <<EOT
 ‣ Modo interactivo…
EOT
}

help-about-shell() {
cat <<EOT
    help shell      Muestra como usar el modo interactivo
EOT
}

help-info-shell() {
  case ${1:-} in
    extended)
cat <<EOT
    Ejecute $(text-colorize bold "${HELP_CMD} shell") y se abrirá una consola interactiva para ejecutar las acciones del proyecto directamente desde la linea de comandos sin tener que especificar el script de ejecución "${0}".

    Se incluirá $(text-colorize bold "${PROJECT_BIN}") en el PATH con lo que permitirá ejecutar todos los scripts en este directorio directamente sin necesidad de especificar la ruta.

    Todos las ejecuciones se aplicaran exclusivamente para el proyecto $(text-colorize bold "${PROJECT_NAME}") mientras este el modo interactivo activo.
    Para salir del modo interactivo, pulse $(text-colorize bold "Ctrl+D") o ejecute $(text-colorize bold "exit")
EOT
    ;;
    *)
cat <<EOT
    shell           Abre una consola interactiva para ejecutar las acciones del proyecto
EOT
    ;;
  esac
}
