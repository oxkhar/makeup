
config-asm() {
  var ASM_PASMO_BIN "${PROJECT_BIN}/pasmo"
  var ASM_PASMO_VERSION "0.5.5"
  var RVM_BIN "${PROJECT_BIN}/rvm"
  var RVM_VERSION "2.0.beta-1.r7"
  var ASM_SOURCE "${PROJECT_PATH}/src"
  var ASM_LIB "${PROJECT_PATH}/lib"
  var ASM_ORIGIN "0x4000"
}

asm-clean() {
  find "${ASM_SOURCE}" "${ASM_LIB}"  -type f "(" -name "*.bin" -o -name "*.symbol"  -o -name "*.public" ")" -delete
}

pasmo() {
  pasmo-install

  local asmFile=${1:?No ASM file}
  local fileName="${asmFile%.*}"
  local asmSrc=$(dirname ${asmFile})

  run "${ASM_PASMO_BIN}" -v -I "${ASM_SOURCE}" -I "${ASM_LIB}" --bin --bracket --alocal "${asmFile}" "${fileName}.bin" "${fileName}.symbol" "${fileName}.public"
}

rvm() {
  rvm-install

  run "${RVM_BIN}" -boot=cpc6128@es -noshader -jump=${ASM_ORIGIN} -load=${ASM_ORIGIN}  "${1}"
}

play() {
  pasmo "${1}" || return $?
  rvm  "${1%.*}".bin || return $?
}

rvm-install() {
  [ -e "${RVM_BIN}" ] && return

  say-info "Installing Retro Virtual Machine ${RVM_VERSION} to ${RVM_BIN}"
  local fileDownload="RetroVirtualMachine.${RVM_VERSION}.linux.x64.zip"
  local urlDownload="https://static.retrovm.org/release/beta1/linux/x64/${fileDownload}"

  download "/tmp/${fileDownload}" "${urlDownload}" || return $?
  (unzip -q -o "/tmp/${fileDownload}" -d /tmp && mv /tmp/RetroVirtualMachine "${RVM_BIN}") || return $?
  rm "/tmp/${fileDownload}"

  chmod +x "${RVM_BIN}"
}

pasmo-install() {
  [ -e "${ASM_PASMO_BIN}" ] && return

  say-info "Installing Pasmo Z80 Assembler ${ASM_PASMO_VERSION} to ${ASM_PASMO_BIN}"
  local fileDownload="pasmo-${ASM_PASMO_VERSION}.tar.gz"
  local urlDownload="https://pasmo.speccy.org/bin/${fileDownload}"

  download "/tmp/${fileDownload}" "${urlDownload}" || return $?
  (tar xvfz "/tmp/${fileDownload}" -C /tmp && cd /tmp/pasmo-${ASM_PASMO_VERSION} && ./configure && make) || return $?
  cp "/tmp/pasmo-${ASM_PASMO_VERSION}/pasmo" "${ASM_PASMO_BIN}"
  cp "/tmp/pasmo-${ASM_PASMO_VERSION}/README" "${PROJECT_PATH}/PASMO.help"
  mkdir -p "${PROJECT_PATH}/examples" && cp "/tmp/pasmo-${ASM_PASMO_VERSION}"/*.asm "${PROJECT_PATH}/examples"
  rm -r /tmp/pasmo-*

  chmod +x "${ASM_PASMO_BIN}"
}
