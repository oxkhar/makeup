
help-title-php() {
cat <<EOT
 ‣ PHP…
EOT
}

help-about-php() {
cat <<EOT
    help php        Ayuda de las acciones de PHP
EOT
}

help-info-php() {
  case ${1:-} in
    extended)
cat <<EOT
    php-cli         Ejecuta PHP como línea de comando

    vendors         Instala las dependencias de PHP
    composer        Ejecuta el gestor de dependencias de PHP

    php-cs-fixer    Corrige el formato del código
    phpstan         Comprobación de las reglas de calidad del código
    phparkitect     Comprobación de las reglas de arquitectura de la aplicación

    phpunit         Herramienta de testing unitario
EOT
    ;;
    *)
cat <<EOT
    vendors         Instala las dependencias de PHP
    composer        Ejecuta el gestor de dependencias de PHP

    phpunit         Herramienta de testing unitario
EOT
    ;;
  esac
}
