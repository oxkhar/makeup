
hey-buddies symfony

###
database() {
  console doctrine:database:drop --force --if-exists "${@}"
  console doctrine:database:create --if-not-exists "${@}"
}

schemas() {
  console doctrine:schema:drop --force "${@}"
  console doctrine:schema:create --no-interaction "${@}"
}

fixtures() {
  console doctrine:fixtures:load --no-interaction "${@}"
}

migrate() {
  console doctrine:migrations:migrate --no-interaction "${@}"
}

migration-diff() {
  console doctrine:migrations:diff --no-interaction "${@}"
}
