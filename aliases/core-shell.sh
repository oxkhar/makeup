
config-core-shell() {
  var ALIASES_SHELL_PROMPT ""
  var ALIASES_SHELL_HEAD "Oxkhar IT ⎼ MakeUp"

  case $- in
    *i*s) ;;
    *i*) set +u; _shell-init;;
  esac
}

shell() {
  _shell-is-interactive && return 0

  BASH_ARGV0="${RUN_BIN}" /bin/bash --init-file "${RUN_BIN}" -i
}

_shell-is-interactive() {
  [ -n "${ALIASES_SHELL_PROMPT}" ]
}

_shell-init() {
  . ~/.bashrc 2> /dev/null || true
#  clear
  export PATH="${PROJECT_BIN}:${PATH}"
  cd "${PROJECT_PATH}"
  core-config-load
  _shell-prompt
  unset shell _shell-init _shell-prompt
}

_shell-prompt() {
  . "${ALIASES_PATH}/omp-main.sh"

  export ALIASES_SHELL_PROMPT=$(
    text-colorize bg_red fg_white bold " ${ALIASES_SHELL_HEAD} "
    text-colorize bg_white fg_red " $(text-capitalize ${PROJECT_NAME}) "
  )

  export PROMPT_COMMAND="aliases-update-prompt \"${ALIASES_SHELL_PROMPT}\""
}

# Experimental
shell-run() {
  local SHELL_CMD=${1:-run}
  local SHELL_DEFAULT=${2:-help}
  local SHELL_NAME=${3:-${SHELL_CMD}}
  local PROMPT="${DOCKER_SERVICE_RUN} ${SHELL_NAME} ('q' to exit): "
  local REPLY=""
  set +e
  read -rp "${PROMPT}"
  while [ "$REPLY" != "q" ]; do
    ${SHELL_CMD} ${REPLY:=${SHELL_DEFAULT}}
    read -rp "${PROMPT}"
  done
}
