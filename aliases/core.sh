: ${ALIASES_DEBUG:=false}

##
core-config() {

  core-config-load

  case $- in
    *i*) ;;
    *) set -euo pipefail;;
  esac

  var ALIASES_PATH "${ALIASES_INSTALL}/aliases"
  var ALIASES_CORE "${ALIASES_PATH}/core.sh"

  var RUN_PATH "$(cd "$(dirname "${0}")" && pwd)"
  var RUN_BIN "${RUN_PATH}/$(basename "${0}")"

  var PROJECT_NAME "$(basename "${PROJECT_PATH}")"
}

core-config-load() {
  local currentPath="$(pwd)"

  var PROJECT_PATH "$(cd "$(dirname "${0}")/.." && pwd)"

  local envFile=""
  local envPath=""
  for envPath in "$(cd "${PROJECT_PATH}/.makeUp" && pwd)" "$(cd "${PROJECT_PATH}/bin" && pwd)" "${PROJECT_PATH}" "${currentPath}"
  do
    for envFile in aliases.dist.env "$(basename "${0}").dist.env"; do
      [ -e "${envPath}/${envFile}" ] && . "${envPath}/${envFile}"
      [ -e "${envPath}/${envFile//.dist/}" ] && . "${envPath}/${envFile//.dist/}"
    done
  done

  var PROJECT_PATH "${PROJECT_PATH}"
  var PROJECT_BIN "${PROJECT_PATH}/bin"
  var ALIASES_INSTALL "$(cd "${PROJECT_PATH}/.makeUp" && pwd)"
  var ALIASES_INSTALL "$(cd "${PROJECT_PATH}/bin" && pwd)"
}

###
ALIASES_INCLUDED_DEEP=0
hey-buddies() {
  [ ${#} -gt 0 ] || _aliases-buddies-available
  (( ++ALIASES_INCLUDED_DEEP ))
  say-debug "Include buddies level: ${ALIASES_INCLUDED_DEEP}"

  local buddieName
  for buddieName in "$@"; do
    # For included buddy only once
    _aliases-is-included "${buddieName}" && continue

    local buddieFile="${ALIASES_PATH}/aliases-${buddieName}.sh"
    [ -e "${buddieFile}" ] \
      || _aliases-buddies-available "${buddieName}" \
      && _aliases-include "${buddieName}"

    source "${buddieFile}"

    _aliases-config-for "${buddieName}"
  done
  [ ${ALIASES_INCLUDED_DEEP} -eq 1 ] && _aliases-configurations
  (( ALIASES_INCLUDED_DEEP-- ))
}

_aliases-buddies-available() {
  local buddieName=${1:-}
  say-error "Ups! No way to find buddy '${buddieName}'"
  say-info "Try with one of these…"
  for b in ${ALIASES_PATH}/aliases-*.sh; do
    buddieName=$(basename "${b}" ".sh")
    say-info "  ${buddieName##bud-}"
  done

  exit 1
}

##
var() {
  local varName=${1?Error: No variable name given}
  local varValue=${2:-}

  eval ": \${${varName}:=\"${varValue}\"}"
  say-debug ": ${varName} = ${!varName}"
}

##
ALIASES_INCLUDED=""
_aliases-include() {
  local buddieName=${1:?Which module do you want to include?}
  ALIASES_INCLUDED+="|${buddieName}|"
  say-debug "Included buddies: ${ALIASES_INCLUDED}"
}

_aliases-is-included() {
  local buddieName=${1:?Which module do you want to know?}
  [ "${ALIASES_INCLUDED/*|${buddieName}|*/${buddieName}}" != "${ALIASES_INCLUDED}" ]
}

_aliases-buddies-included() {
  echo "${ALIASES_INCLUDED//|/ }"
}

##
ALIASES_CONFIGS=()
_aliases-configurations() {
  ## Load config modules
  for configurator in "${ALIASES_CONFIGS[@]}"; do
    say-debug "Loading configuration '${configurator}'"

    type -t "${configurator}" >/dev/null &&
      ${configurator} ||
      say-debug "No configuration found for '${configurator}'"
  done

  ALIASES_CONFIGS=()
}

_aliases-config-for() {
  local configModule=${1:?Which module do you want to get help for?}

  ALIASES_CONFIGS+=("config-${@}")
}

##
aliases-debug() {
  [[ "${ALIASES_DEBUG:-false}" == "true" ]]
}

say-debug() {
  aliases-debug && echo "DEBUG🔈${@}" >&2 || true
}

_aliases-core-init() {
  _aliases-is-included core && return
  _aliases-include core

  core-config

  for c in ${ALIASES_PATH}/core-*; do
    source "${c}"
    _aliases-config-for "$(basename "${c}" ".sh")"
  done
  _aliases-configurations
}

_aliases-core-init
