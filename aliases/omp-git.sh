
function aliases-git-prompt() {
    # if not is a git project exit
    aliases-git_repo_is_available || return 0
    # Config prompt symbols
    local  \
        prompt_prefix="[⛕ "       \
        prompt_head=""          \
        prompt_stashes="⚑"      \
        prompt_remote=" "       \
        prompt_ahead="↑·"       \
        prompt_behind="↓·"      \
        prompt_separator="|"    \
        prompt_action="~"       \
        prompt_staged="●"       \
        prompt_conflicts="✖"    \
        prompt_changed="✚"      \
        prompt_untracked="…"    \
        prompt_clean="✔"        \
        prompt_suffix="]"
    # Config colors
    local  \
        color_prefix=""                 \
        color_head="fg_purple bold"     \
        color_stashes="fg_blue bold"    \
        color_remote="fg_white bold"    \
        color_ahead=""                  \
        color_behind=""                 \
        color_separator=""              \
        color_action="fg_cyan bold"     \
        color_staged="fg_yellow"        \
        color_conflicts="fg_red"        \
        color_changed="fg_blue"         \
        color_untracked="fg_white"      \
        color_clean="fg_green bold"     \
        color_suffix=""
    # Status values
    local action head commit branch remote ref_merge ref_remote
    local -i ahead behind stashes stashes_on_branch  \
             changed clean conflicts staged untracked

     # Generate status data
    read head commit branch <<< $(aliases-git_current_head)
    read stashes stashes_on_branch <<< $(aliases-git_stashes "$branch")
    read remote ref_remote ref_merge <<< $(aliases-git_remote_refs "$branch")
    read ahead behind <<< $(aliases-git_remote_diffs "$ref_remote" "$ref_merge")
    read action <<< $(aliases-git_current_action)
    read clean changed staged untracked conflicts <<< $(aliases-git_status_files)

    # Build prompt
    text-format prefix
    text-format head

    (( stashes_on_branch > 0 )) && color_stashes+=" underline"
    text-format_if not_zero stashes

    text-format_if not_empty remote
    text-format_if not_zero behind
    text-format_if not_zero ahead

    text-format separator "{prompt}"

    text-format_if not_empty action "{prompt}{value}{prompt}"

    text-format_if not_zero clean "{prompt}"
    text-format_if not_zero staged
    text-format_if not_zero conflicts
    text-format_if not_zero changed
    text-format_if not_zero untracked

    text-format suffix
}

function aliases-git_repo_is_available() {
    git rev-parse --git-dir 2> /dev/null 1>&2
}

# @return head Reference of actual position of head
# @return commit Current sort SHA1
# @return branch Branch name where we stay
function aliases-git_current_head() {
    local commit=$(git rev-parse --short HEAD 2> /dev/null)

    local branch=$(git symbolic-ref -q HEAD 2> /dev/null)
    branch=${branch/refs\/heads\//}

    local head=$branch
    if [[ -z $head ]]; then
        head=":"$commit
    fi

    omp-debug && (
        text-colorize fg_green "Head:"; text-colorize " $head "
        echo
    ) >&2

    echo -n "$head" "$commit" "$branch"
}

# @param $1 Branch to search stashes
# @return stashes Number stashes saved
# @return stashes_on_branch Number stashes over a given branch
function aliases-git_stashes() {
    local branch=${1?'param "branch" is null'}

    local -i stashes_on_branch=0
    local -i stashes=$(git stash list 2> /dev/null | wc -l)

    if [[ -n $branch && $stashes -gt 0 ]]; then
        stashes_on_branch=$(git stash list 2> /dev/null | grep " [Oo]n ${branch}:" | wc -l)
    fi

    omp-debug && (
        text-colorize fg_green "Stashes:"; text-colorize " $stashes_on_branch/$stashes "
        echo
    ) >&2

    echo -n $stashes $stashes_on_branch
}

# @param $1 Reference to remote
# @param $2 Reference to merge
# @return ahead Number of local commits ahead from remote repository
# @return behind Number of local commits behind from remote repository
function aliases-git_remote_diffs() {
    local ref_remote=${1?'param "ref_remote" is null'}
    local ref_merge=${2?'param "ref_merge" is null'}
    local -i ahead behind
    local list_rev

    list_rev=$(git rev-list --left-right ${ref_remote}...HEAD 2> /dev/null)
    if (( $? == 128 )); then
        list_rev=$(git rev-list --left-right ${ref_merge}...HEAD 2> /dev/null)
    fi

    ahead=$(  grep "^>"    <<< "$list_rev" | wc -l )
    behind=$( grep -Ev "(^>|^$)" <<< "$list_rev" | wc -l )

    omp-debug && (
        text-colorize fg_red "Ahead:"; text-colorize " $ahead "
        text-colorize fg_red "Behind:"; text-colorize " $behind "
        echo
    ) >&2

    echo -n $ahead $behind
}

# @param $1 Branch to search remote info
# @return remote Name remote repository associated at branch
# @return ref_merge Reference of branch local to merge from repository
# @return ref_remote Reference of remote branch associted
function aliases-git_remote_refs() {
    local branch=${1?'param "branch" is null'}
    local remote ref_remote ref_merge

    [[ -z $branch ]] && return 0

    remote=$(git config  branch.${branch}.remote)
    if [[ -n $remote ]]; then
        ref_merge=$(git config branch.${branch}.merge)
    else
        remote="."
        ref_merge=$(git rev-parse --symbolic-full-name ${branch} 2> /dev/null)
    fi

    if [[ $remote == "." ]]; then
        ref_remote=$ref_merge
    else
        ref_remote=$(git rev-parse --symbolic-full-name ${remote}/${branch} 2> /dev/null)
    fi

    omp-debug && (
        text-colorize fg_yellow "Remote:"; text-colorize " $remote "
        text-colorize fg_yellow "Merge:"; text-colorize " $ref_merge "
        text-colorize fg_yellow "Ref:"; text-colorize " $ref_remote "
        echo
    ) >&2

    echo -n "$remote" "$ref_remote" "$ref_merge"
}

# @return action Current command in action (merging, rebase, ...)
function aliases-git_current_action () {
    local action=""

    # Inside git dir don't show any status
    if [[ $(git rev-parse --is-inside-git-dir) == "true" ]]; then
        echo -n "GIT_DIRECTORY"
        return 0
    fi

    local git_dir="$(git rev-parse --git-dir)"

    if [ -f "$git_dir/rebase-merge/interactive" ]; then
        action="REBASE_INTERACTIVE"
    elif [ -d "$git_dir/rebase-merge" ]; then
        action="REBASE_MERGE"
    elif [ -d "$git_dir/rebase-apply" ]; then
        if [ -f "$git_dir/rebase-apply/rebasing" ]; then
            action="REBASE"
        elif [ -f "$git_dir/rebase-apply/applying" ]; then
            action="MAILBOX"
        else
            action="MAILBOX/REBASE"
        fi
    elif [ -f "$git_dir/MERGE_HEAD" ]; then
        action="MERGE"
    elif [ -f "$git_dir/CHERRY_PICK_HEAD" ]; then
        action="CHERRY-PICK"
    elif [ -f "$git_dir/BISECT_LOG" ]; then
        action="BISECT"
    fi

    omp-debug && (
        text-colorize fg_cyan "Action:"; text-colorize " $action "
        echo
    ) >&2

    echo -n "$action"
}

# @return clean Value one when there is no changes in any files
# @return changed Number of files modified in work area
# @return staged Number os files added in stage area
# @return untracked Number of new files without add to repository
# @return conflicts Number of files with some conflicts when were merged
function aliases-git_status_files() {
    [[ $(git rev-parse --is-inside-git-dir) == "true" ]] && return 0

    local -i clean changed staged untracked conflicts

    local status=$(git status --porcelain 2> /dev/null) || return 0

    staged=$(    grep -E "(^[MARC][ MD]|^[D][ M])" <<< "$status" | wc -l )
    changed=$(   grep    "^[ MARC][MD]"            <<< "$status" | wc -l )
    untracked=$( grep    "^??"                     <<< "$status" | wc -l )
    conflicts=$( grep -E "(^U|^.U|^DD|^AA)"        <<< "$status" | wc -l )

    if (( $staged == 0 && $conflicts == 0 && $changed == 0 && $untracked == 0 )); then
        clean=1
    else
        clean=0
    fi

    omp-debug && (
        text-colorize fg_purple "Staged:"; text-colorize " $staged "
        text-colorize fg_purple "Conflicts:"; text-colorize " $conflicts "
        text-colorize fg_purple "Modified:"; text-colorize " $changed "
        text-colorize fg_purple "Untracked:"; text-colorize " $untracked "
        text-colorize fg_purple "Clean:"; text-colorize " $clean "
        echo
    ) >&2

    echo -n $clean $changed $staged $untracked $conflicts
}

function omp-debug() {
    [[ "${OMP_DEBUG:-}" == "true" ]]
}
