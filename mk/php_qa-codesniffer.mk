
## Params
PHP_CODESNIFFER_VERSION ?= 3.6.0
PHP_CODESNIFFER_OPTIONS ?= #
PHP_CODEFIX_OPTIONS     ?= #

## Config
PHP_CODESNIFFER_BIN    = ${BIN}/phpcs
PHP_CODESNIFFER        = $(PHP_CLI) ${APP_BIN}/phpcs
PHP_CODESNIFFER_ARGS   = --report=summary

PHP_CODESNIFFER_CONFIG = .phpcs.xml
PHP_CODESNIFFER_REPORT = ${REPORT}/codesniffer

PHP_CODEFIX_BIN        = ${BIN}/phpcbf
PHP_CODEFIX            = $(PHP_CLI) ${APP_BIN}/phpcbf
#
CONF_FILES += ${PHP_CODESNIFFER_CONFIG}
UNINSTALL_FILES += ${PHP_CODESNIFFER_BIN} ${PHP_CODEFIX_BIN}

QA_TOOLS_BIN       += ${PHP_CODESNIFFER_BIN}
QA_TARGETS_ANALYSE += phpcs-diff phpcs
QA_TARGETS_REPORTS += phpcs-report
##
phpcs-report: PHP_CODESNIFFER_ARGS := \
		--report-xml=${PHP_CODESNIFFER_REPORT}/report.xml \
		--report-checkstyle=${PHP_CODESNIFFER_REPORT}/checkstyle.xml \
		--report-json=${PHP_CODESNIFFER_REPORT}/report.json \
		--report-junit=${PHP_CODESNIFFER_REPORT}/junit.xml \
		--report-gitblame=${PHP_CODESNIFFER_REPORT}/gitblame.txt

phpcs-report: ${PHP_CODESNIFFER_CONFIG} ${REPORT} | ${PHP_CODESNIFFER_BIN}
	$I PHP CodeSniffer reports…
	mkdir -p ${PHP_CODESNIFFER_REPORT}

	$(PHP_CODESNIFFER) ${PHP_CODESNIFFER_ARGS} ${PHP_CODESNIFFER_OPTIONS} > /dev/null

	echo -e "Builded..."

phpcs-diff: PHP_CODESNIFFER_ARGS := --report=diff
phpcs-diff: phpcs                                          ## PHP CodeSniffer diffs for fix code

phpcs: ${PHP_CODESNIFFER_CONFIG} | ${PHP_CODESNIFFER_BIN}  ## PHP CodeSniffer
	$I PHP CodeSniffer…

	$(PHP_CODESNIFFER) ${PHP_CODESNIFFER_ARGS} ${PHP_CODESNIFFER_OPTIONS}

${PHP_CODESNIFFER_BIN}: | ${BIN}
	$I installing PHP CodeSniffer…

	$(DOWNLOAD) ${PHP_CODESNIFFER_BIN} \
		https://github.com/squizlabs/PHP_CodeSniffer/releases/download/${PHP_CODESNIFFER_VERSION}/phpcs.phar \
		|| exit $$?
	$(DOWNLOAD) ${PHP_CODEFIX_BIN} \
		https://github.com/squizlabs/PHP_CodeSniffer/releases/download/${PHP_CODESNIFFER_VERSION}/phpcbf.phar \
		|| exit $$?
	chmod +x ${PHP_CODESNIFFER_BIN} ${PHP_CODEFIX_BIN}

