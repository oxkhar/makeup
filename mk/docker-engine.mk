### Params
DOCKER_ENV_CONFIG    ?= docker.env
DOCKER_DOT_ENV_FILES ?= ${DOT_ENV_FILES} ${DOCKER_ENV_CONFIG}

DOCKER_ENV_FILE		 ?= .env.docker
-include ${DOCKER_ENV_FILE}

DOCKER_TTY_MODE      ?= ALLOCATE # Values: ALLOCATE (interactive) | ATTACH (use output)

# Docker image where to execute project source
DOCKER_REGISTRY      ?= # Have to end with / (slash) eg.: registry.gitlab.com/
DOCKER_IMAGE_NAME    ?= ${APP_PROJECT}/${APP_NAME}
DOCKER_IMAGE_VERSION ?= ${APP_VERSION}

DOCKER_IMAGE_RUN     ?= ${DOCKER_REGISTRY}${DOCKER_IMAGE_NAME}:${DOCKER_IMAGE_VERSION}
DOCKER_IMAGE_BUILD   ?= ${DOCKER_IMAGE_RUN}
DOCKER_BUILD_CONTEXT ?= .

DOCKER_USER_RUN      ?= $(shell id -u):$(shell id -g)
DOCKER_WORKDIR       ?= /app
DOCKER_PLUGINS       ?= ~/.docker/cli-plugins

### Config
DOCKER_BIN = $(shell which docker)

# Execute project with Docker CLI or Docker Compose, assigned in DOCKER_RUN and DOCKER_BUILD...
ifndef DOCKER_RUN
DOCKER_RUN    = ${DOCKER_CLI_RUN}
DOCKER_BUILD ?= ${DOCKER_CLI_BUILD}
endif

#SSH_PRIVATE_KEY_FILE := ${HOME}/.ssh/id_rsa
#DOCKER_BUILD_PARAMS  := --build-arg SSH_PRIVATE_KEY="$(shell cat ${SSH_PRIVATE_KEY_FILE} 2> /dev/null)"
DOCKER_BUILD_PARAMS	 += --build-arg APP_USER="$(shell id -u):$(shell id -g)"

# To interact with term in Docker runs
DOCKER_TTY_ALLOCATE = --tty --interactive
# To can redirect output in pipelines
DOCKER_TTY_ATTACH   = --interactive --attach stderr --attach stdout
DOCKER_TTY_OPTIONS  = ${DOCKER_TTY_${DOCKER_TTY_MODE}}

DOCKER_RUN_OPTIONS += --rm --workdir ${DOCKER_WORKDIR}

ifeq (${MKUP_MODE},devops)
DOCKER_TTY_MODE      = ATTACH
else
DOCKER_RUN_OPTIONS += --user ${DOCKER_USER_RUN} \
					--volume /etc/passwd:/etc/passwd:ro \
					--volume /etc/group:/etc/group:ro \
					--volume ${CURDIR}:${DOCKER_WORKDIR}
endif
# Docker CLI
DOCKER_CLI_BUILD = $(DOCKER_BIN) build \
				${DOCKER_BUILD_PARAMS} \
				-t ${DOCKER_IMAGE_BUILD} \
				${DOCKER_BUILD_CONTEXT}

DOCKER_CLI_RUN = $(DOCKER_BIN) run \
				${DOCKER_RUN_OPTIONS} \
				${DOCKER_TTY_OPTIONS} \
				${DOCKER_IMAGE_RUN}
				## --env-file ${DOCKER_ENV_FILE} \
#
UNINSTALL_FILES += ${DOCKER_ENV_FILE}
APP_TARGETS     += docker-build docker-env docker
DEVELOP_TARGETS += docker-install

##
install-dev-env: docker-install

docker-install: docker-env

ifeq (${DOCKER_BIN},)
docker-install: docker

docker: | ${BIN}
	$I installing docker…
	$(DOWNLOAD) ${BIN}/get-docker.sh https://get.docker.com
	sh ${BIN}/get-docker.sh || false
	sleep 1
	rm -f ${BIN}/get-docker.sh
	sudo adduser $(shell whoami) docker
	sg docker "docker ps -q" || false
	sg docker "make docker-build"
	echo -e "\n\n[!]\n[!] Docker installation stop current Make process"
	echo -e "[!]\n[!] Restart your session to apply group changes and current user can use Docker"
	echo -e "[!]\n[!] Run again \"make ${MAKECMDGOALS}\" to finish \n[!]\n"
	sg docker "newgrp $(shell whoami)" && false
else
docker-install:
	make docker-build
endif

docker-env:
	$I Docker environment ${APP_ENV}…
	. ${MKUP_PATH}./aliases-install.sh
	aliases-docker-env ${DOCKER_DOT_ENV_FILES} > "${DOCKER_ENV_FILE}"

docker-build: docker-env			## Build docker images
	$I creating docker images…
	$(DOCKER_BUILD)
