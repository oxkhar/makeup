# If you include this file then the file "docker-engine.mk" is not necessary

### Params
# Service name in docker compose to execute project source
DOCKER_COMPOSE_VERSION  ?= v2.15.1
DOCKER_SERVICE_RUN      ?= cli
DOCKER_SERVICE_BUILD    ?= ${DOCKER_SERVICE_RUN}

DOCKER_COMPOSE_FILE     ?= docker-compose.yaml

DOCKER_COMPOSE_SERVICES	?=

DOCKER_COMPOSE_ENGINE ?= PLUGIN  # Values: PLUGIN | STANDALONE

### Config
DOCKER_COMPOSE_BIN_PLUGIN = ${DOCKER_PLUGINS}/docker-compose
DOCKER_COMPOSE_PLUGIN     = $(DOCKER_BIN) compose

DOCKER_COMPOSE_BIN_STANDALONE = ${BIN}/docker-compose
DOCKER_COMPOSE_STANDALONE     = $(DOCKER_COMPOSE_STANDALONE_BIN)

DOCKER_COMPOSE_BIN = $(DOCKER_COMPOSE_BIN_$(strip $(DOCKER_COMPOSE_ENGINE)))
DOCKER_COMPOSE     = $(DOCKER_COMPOSE_$(strip $(DOCKER_COMPOSE_ENGINE))) \
						--file ${DOCKER_COMPOSE_FILE} \
						--env-file ${DOCKER_ENV_FILE}

DOCKER_COMPOSE_RUN   = $(DOCKER_COMPOSE) run ${DOCKER_RUN_OPTIONS} ${DOCKER_SERVICE_RUN}
DOCKER_COMPOSE_BUILD = $(DOCKER_COMPOSE) build ${DOCKER_BUILD_PARAMS} ${DOCKER_SERVICE_BUILD}

# Execute project with Docker CLI or Docker Compose, assigned in DOCKER_RUN and DOCKER_BUILD...
DOCKER_RUN   = ${DOCKER_COMPOSE_RUN}
DOCKER_BUILD = ${DOCKER_COMPOSE_BUILD}
#
UNINSTALL_FILES += ${DOCKER_COMPOSE_BIN_STANDALONE}
APP_TARGETS	    += start stop down logs

## Running
docker-install docker-build: | ${DOCKER_COMPOSE_BIN}

start: ${DOCKER_ENV_FILE}        ## Start processes to run application
	$I start server…
	$(DOCKER_COMPOSE) up -d ${DOCKER_COMPOSE_SERVICES}

stop:                            ## Stop current running application
	$I stop server…
	-$(DOCKER_COMPOSE) stop

down:                            ## Stop and remove all
	$I stop and remove all…
	$(DOCKER_COMPOSE) down

logs:                            ## Show logs generated
	-$(DOCKER_COMPOSE) logs -f

#
${DOCKER_COMPOSE_BIN_PLUGIN}: ${DOCKER_COMPOSE_BIN_STANDALONE}
	$I installing docker compose plugin…
	mkdir -p $$(dirname ${DOCKER_COMPOSE_BIN_PLUGIN})
	cp ${DOCKER_COMPOSE_BIN_STANDALONE} ${DOCKER_COMPOSE_BIN_PLUGIN}
	chmod +x ${DOCKER_COMPOSE_BIN_PLUGIN}

${DOCKER_COMPOSE_BIN_STANDALONE}: ${BIN}
	$I installing docker compose standalone…
	$(DOWNLOAD) ${DOCKER_COMPOSE_BIN_STANDALONE} \
		https://github.com/docker/compose/releases/download/v${DOCKER_COMPOSE_VERSION:v%=%}/docker-compose-`uname -s`-`uname -m` \
		|| exit $$?
	chmod +x ${DOCKER_COMPOSE_BIN_STANDALONE}

##
include ${MKUP_PATH}./docker-engine.mk
