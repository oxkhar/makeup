## Params
PHP_COMPOSER_VERSION ?= 2.5.7

VENDOR_BIN           ?= ${BIN}

COMPOSER_OPTIONS     ?= # --ignore-platform-reqs --optimize-autoloader
COMPOSER_HOME        ?= /home/composer

## Config
COMPOSER_BIN = ${BIN}/composer
COMPOSER     = $(PHP_CLI) ${APP_BIN}/composer

COMPOSER_PLUGINS = vendor/bamarni/composer-bin-plugin
#
DOCKER_BUILD_PARAMS	+= --build-arg COMPOSER_HOME="${COMPOSER_HOME}"
#
UNINSTALL_FILES += ${COMPOSER_BIN} vendor

COMPOSER_TARGETS = vendor vendor-update composer-plugins
APP_TARGETS += ${COMPOSER_TARGETS}
##
ifneq (${MKUP_MODE},devops)
${COMPOSER_BIN} ${COMPOSER_TARGETS}: DOCKER_RUN_OPTIONS += \
					-e COMPOSER_HOME="${COMPOSER_HOME}" \
					-v composer:"${COMPOSER_HOME}":cached
endif

composer-plugins: ${COMPOSER_BIN} ${COMPOSER_PLUGINS}
${COMPOSER_PLUGINS}:
	$I installing composer plugins…
	$(COMPOSER) config allow-plugins.bamarni/composer-bin-plugin true
	$(COMPOSER) config extra.bamarni-bin.target-directory "vendor/.vendor-bin"
	$(COMPOSER) require bamarni/composer-bin-plugin

install: vendor
update: vendor-update

vendor: composer.json | ${COMPOSER_BIN}         ## Deploy dependencies
	$I generate dependencies…
	$(COMPOSER) install ${COMPOSER_OPTIONS} || exit 1

vendor-update: | ${COMPOSER_BIN}                ## Force update dependencies
	$I updating all dependencies…
	$(COMPOSER) update ${COMPOSER_OPTIONS} || exit 1

composer.json: | ${COMPOSER_BIN}
	$I create composer config…
	$(COMPOSER) init --no-interaction \
		--type project \
		--license WTFPL \
		--stability stable \
		--name "${APP_PROJECT}/${APP_NAME}"
	$(COMPOSER) config bin-dir bin

${COMPOSER_BIN}: | ${BIN}
	$I installing composer…
	$(DOWNLOAD) ${COMPOSER_BIN} https://getcomposer.org/download/${PHP_COMPOSER_VERSION:v%=%}/composer.phar
