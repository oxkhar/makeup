
## Params
PHPMD_VERSION ?= 2.10.1
PHPMD_RULES   ?= cleancode,codesize,controversial,design,naming,unusedcode
PHPMD_OPTIONS ?= # --ignore-violations-on-exit

## Config
PHPMD_BIN    = ${BIN}/phpmd
PHPMD        = $(PHP_CLI) ${APP_BIN}/phpmd
PHPMD_ARGS   = ansi
PHPMD_REPORT = ${REPORT}/phpmd
#
UNINSTALL_FILES    += ${PHPMD_BIN}

QA_TOOLS_BIN       += ${PHPMD_BIN}
QA_TARGETS_ANALYSE += phpmd
QA_TARGETS_REPORTS += phpmd-report
##
phpmd-report: ${REPORT} | ${PHPMD_BIN}
	$I PHP Mess Detector reports

	mkdir -p ${PHPMD_REPORT}

	$(PHPMD) ${SOURCE} html ${PHPMD_RULES} ${PHPMD_OPTIONS} --reportfile ${PHPMD_REPORT}/index.html
	$(PHPMD) ${SOURCE} json ${PHPMD_RULES} ${PHPMD_OPTIONS} --reportfile ${PHPMD_REPORT}/report.json
	$(PHPMD) ${SOURCE} text ${PHPMD_RULES} ${PHPMD_OPTIONS} --reportfile ${PHPMD_REPORT}/report.txt
	$(PHPMD) ${SOURCE} xml ${PHPMD_RULES} ${PHPMD_OPTIONS} --reportfile ${PHPMD_REPORT}/report.xml

	echo -e "Builded..."

phpmd: | ${PHPMD_BIN}                  ## PHP Mess Detector
	$I PHP Mess Detector

	$(PHPMD) ${SOURCE} ${PHPMD_ARGS} ${PHPMD_RULES} ${PHPMD_OPTIONS} || true

${PHPMD_BIN}: | ${BIN}
	$I Installing PHP Mess Detector…

	$(DOWNLOAD) ${PHPMD_BIN} \
		https://github.com/phpmd/phpmd/releases/download/${PHPMD_VERSION}/phpmd.phar \
		|| exit $$?
	chmod +x ${PHPMD_BIN}
