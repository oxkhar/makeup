
## Params
PHPUNIT_OPTIONS	?= --colors

## Config
PHPUNIT_BIN  = ${VENDOR_BIN}/phpunit
PHPUNIT      = $(PHP_CLI) ${PHPUNIT_BIN}
PHPUNIT_ARGS =
#
TEST_TARGETS := tests-verbose tests-coverage tests-report

QA_TARGETS_REPORTS += tests-report
QA_REPORTS_TESTS    = phpmetrics-tests tests-report
#
UNINSTALL_FILES += ${PHPUNIT_BIN}

APP_TARGETS += tests ${TESTS_TARGETS}
##
tests:                                          ## Run tests
	$I running $(TEST_NAME:tests-%=% ) tests…
	$(PHPUNIT) ${PHPUNIT_OPTIONS} ${PHPUNIT_ARGS} ${TEST_FILES}

tests-verbose:  PHPUNIT_ARGS = --testdox         ## Run tests showing description

tests-coverage: PHP_CLI_ARGS = ${XDEBUG_ENABLE}           ## Run tests with coverage reporting
tests-coverage: PHPUNIT_ARGS = --coverage-text --testdox

tests-report:   PHP_CLI_ARGS = ${XDEBUG_ENABLE}           ## Run tests building report artifacts
tests-report:   PHPUNIT_ARGS = \
                 --coverage-html ${REPORT}/coverage/ \
                 --coverage-clover ${REPORT}/coverage.xml \
                 --coverage-xml ${REPORT}/coverage-xml \
                 --coverage-php ${REPORT}/coverage.serialized \
                 --log-junit ${REPORT}/junit.xml \
                 --testdox-html ${REPORT}/testdox.html \
                 --testdox-text ${REPORT}/testdox.txt

${TEST_TARGETS}: TEST_NAME := $(filter test-%, ${MAKECMDGOALS})
${TEST_TARGETS}: TEST_FILES ?= $(filter tests/%, ${MAKECMDGOALS})
${TEST_TARGETS}: tests
